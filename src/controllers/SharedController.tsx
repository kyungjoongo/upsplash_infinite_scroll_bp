import { makeAutoObservable, observable } from "mobx";
import Toast from "react-native-root-toast";
import axios from "axios";
import _ from "lodash";

class SharedService {
    constructor() {
        makeAutoObservable(this);
    }

    counter = 0;
    loading = false;

    enterLoading = false;


    refList: any = [];

    setRefList(arrList: any) {
        this.refList = arrList;
    }

    videoLoading = false;

    videoList: any = [];

    setVideoList(arrList: any) {
        this.videoList = arrList;
        console.log("setVideoList-===>", _.cloneDeep(this.videoList));
    }

    currentVideoIndex = 0;

    prevVideoIndex = 0;

    setPrevVideoIndex(value:any){
        this.prevVideoIndex= value;
    }

    setCurrentVideoIndex(value: any) {
        this.currentVideoIndex = value;
        console.log("setCurrentVideoIndex-===>", this.currentVideoIndex);
    }

    incrementDoubleCount() {
        this.counter = this.counter + 2;
        console.log("increment2", this.counter);
    }

    incrementCount() {
        this.counter++;
    }

    toggleLoading(value: boolean = false) {
        this.loading = !this.loading;
    }

    arrList: any = [];

    async getList() {
        this.loading = true;
        let results = await axios({
            url: "https://jsonplaceholder.typicode.com/posts",
            method: "get",
        }).then(res => {
            console.log("temp-===>", res);
            return res.data;
        });
        this.loading = false;
        this.arrList = results;
    }
}

const sharedController = new SharedService();

export default sharedController;
