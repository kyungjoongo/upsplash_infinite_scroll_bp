import { Appearance, Button, Platform, TextInput, TouchableOpacity, View } from "react-native";
import { sharedStyles } from "../styles/SharedStyles";
import React from "react";
import sharedService from "../services/SharedService";
// @ts-ignore
import Icon from "react-native-vector-icons/FontAwesome";


type TopSearchBarProps = {
    onChangeInputText: any,
    inputText: any,
    handleSubmitEditing: any,
    setTextInputFocused: any,
    colCount: number,
    setColCount: any,

};

export default function TopSearchBar({
                                         onChangeInputText,
                                         inputText,
                                         handleSubmitEditing,
                                         setTextInputFocused,
                                         colCount,
                                         setColCount,
                                     }: TopSearchBarProps) {

    return (
        <View style={{ position: "absolute", top: 0, flexDirection: "row" }}>
            <View style={{ flexDirection: "row", flex: .2, height: 30, alignSelf: "center" }}>
                <View style={{ width: 8 }} />
                <TouchableOpacity
                    onPress={() => {
                        setColCount(1);
                    }}
                >
                    <Icon name="align-justify" size={30} color="orange" style={{ marginTop: 1 }} />
                </TouchableOpacity>
                <View style={{ width: 10 }} />
                <TouchableOpacity
                    onPress={() => {
                        setColCount(2);
                    }}
                >
                    <Icon name={"columns"} size={30} color="orange" style={{ marginTop: 0 }} />
                </TouchableOpacity>

            </View>
            <View style={{ flex: .6 }}>
                <TextInput
                    style={[sharedStyles.input,
                        {
                            backgroundColor: sharedService.isDarkMode() ? "white" : "#cbc3c3",
                            color: !sharedService.isDarkMode() ? "black" : "black",
                        }]}
                    onChangeText={onChangeInputText}
                    value={inputText}
                    onSubmitEditing={handleSubmitEditing}
                    onFocus={() => {
                        setTextInputFocused(true);
                    }}
                    onEndEditing={() => {
                        setTextInputFocused(false);
                    }}
                    numberOfLines={1}
                    clearButtonMode={"always"}
                />
            </View>
            <View style={sharedStyles.searchBtnContainer}>
                <Button color={"orange"} title={"Search"}
                        onPress={handleSubmitEditing}
                />
            </View>

        </View>
    );
}
