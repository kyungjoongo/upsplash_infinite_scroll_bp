import { ActivityIndicator, Dimensions, FlatList, Image, TouchableOpacity, View } from "react-native";
import React, { useEffect } from "react";
import { TypeUnsplashItem } from "../types/Types";
// @ts-ignore
import Icon from "react-native-vector-icons/FontAwesome";
import { sharedStyles } from "../styles/SharedStyles";

type ImageFlatListProps = {
    flatListRef: any,
    imageList: any,
    setCurrentItem: any,
    setCurrentIdx: any,
    setModalVisible: any,
    handleLoadMore: any,
    flatListHeight: any,
    bottomLoading: boolean,
    colCount: any,
    setColCount: any,
};


export default function ImageFlatList(props: ImageFlatListProps) {
    let {
        flatListRef,
        imageList,
        setCurrentItem,
        setCurrentIdx,
        setModalVisible,
        handleLoadMore,
        flatListHeight,
        bottomLoading,
        colCount,
        setColCount,
    } = props;

    useEffect(() => {
    }, props.imageList);

    const renderItemOne = (itemOne: TypeUnsplashItem) => (
        <TouchableOpacity
            key={itemOne.toString()}
            activeOpacity={0.9}
            onPress={() => {
                setCurrentItem(itemOne.item);
                setCurrentIdx(itemOne.index);
                setModalVisible(true);
            }}
            style={sharedStyles.imageOneContainer}>
            <Image
                style={[
                    {
                        width: Dimensions.get("window").width / colCount,
                        height: flatListHeight * 0.37,
                    },
                    sharedStyles.imageOne,
                ]}
                resizeMethod={"scale"}
                source={{
                    uri: itemOne.item.urls.small,
                }}
            />
        </TouchableOpacity>
    );

    return (
        <>
            <FlatList
                refreshing={true}
                indicatorStyle={"black"}
                initialScrollIndex={0}
                numColumns={colCount}
                key={colCount}
                //removeClippedSubviews={true}
                //disableVirtualization={false}
                //getItemLayout={getItemLayout}
                ref={flatListRef}
                data={imageList}
                keyExtractor={(item) => item.id}
                renderItem={renderItemOne}
                initialNumToRender={5}
                maxToRenderPerBatch={10}
                windowSize={10}
                onEndReachedThreshold={0.3}
                viewabilityConfig={{
                    minimumViewTime: 300,
                    viewAreaCoveragePercentThreshold: 100,
                }}
                onEndReached={({ distanceFromEnd }: any) => {
                    handleLoadMore();
                }}
                ListFooterComponent={
                    bottomLoading ?
                        <View style={{ marginBottom: 5, zIndex: 9999, width: "100%", justifyContent: "center" }}>
                            <ActivityIndicator color={"green"} size={"large"} /></View> : null
                }

            />
        </>
    );
}
