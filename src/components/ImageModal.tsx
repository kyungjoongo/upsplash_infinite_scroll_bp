import { ActivityIndicator, Image, Modal, Text, TouchableOpacity, View } from "react-native";
import React, { useState } from "react";
//@ts-ignore
import { getStatusBarHeight, ifIphoneX, isIphoneX } from "react-native-iphone-x-helper";
import { sharedStyles } from "../styles/SharedStyles";
import sharedService from "../services/SharedService";
//@ts-ignore
import Icon from "react-native-vector-icons/FontAwesome";
import { TypeItemOne } from "../types/Types";

type FullImageModalProps = {
    modalVisible: boolean,
    setModalVisible: any,
    currentIdx: number,
    currentItem: TypeItemOne,
    flatListRef: any,
    colCount: any,
    setColCount: any,
};

export default function ImageModal({
                                       modalVisible,
                                       setModalVisible,
                                       currentIdx,
                                       currentItem,
                                       flatListRef,
                                       colCount,
                                       setColCount,
                                   }: FullImageModalProps) {


    const handleModalClose = () => {
        setModalVisible(false);
        flatListRef.current.scrollToIndex({
            animated: true,
            index: colCount === 1 ? currentIdx : Math.floor(currentIdx / 2),
        });
    };

    const [loading, setLoading] = useState(true);

    return (
        <Modal
            animationType="slide"
            transparent={false}
            visible={modalVisible}
            onRequestClose={handleModalClose}
            style={{}}
        >
            <View
                style={[sharedStyles.modalTitleContainer]}
            >
                <View style={{ flex: .9 }}>
                    <Text
                        style={sharedStyles.modalTitle}> {currentItem !== undefined && currentItem.alt_description}</Text>
                </View>
                <TouchableOpacity style={{ flex: .1, marginTop: -5, marginLeft: 18 }}
                                  onPress={handleModalClose}
                >
                    <Icon name={"close"} size={30} color="orange" style={{}} />
                </TouchableOpacity>
            </View>
            {loading && <ActivityIndicator color={"orange"} size={"large"} />}
            {currentItem !== undefined &&
            <>
                <Image
                    onLoadEnd={() => {
                        setLoading(false);
                    }}

                    style={{
                        width: "100%",
                        height: "100%",
                    }}
                    resizeMethod={"resize"}
                    source={{
                        uri: currentItem.urls.regular,
                    }}
                />
            </>
            }
        </Modal>
    );
}
