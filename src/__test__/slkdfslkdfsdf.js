var axios = require("axios").default;

var options = {
    method: 'GET',
    url: 'https://youtube-v31.p.rapidapi.com/search',
    params: {
        q: 'justin bierber',
        part: 'snippet,id',
        regionCode: 'US',
        maxResults: '50',
        order: 'date'
    },
    headers: {
        'x-rapidapi-host': 'youtube-v31.p.rapidapi.com',
        'x-rapidapi-key': 'jnCM8tDHT9msh3e8CMScS9crMJ4Mp1SVdaYjsnPc7f8GVxTtbP'
    }
};

axios.request(options).then(function (response) {
    console.log(response.data);
}).catch(function (error) {
    console.error(error);
});
