import React, { Props, useState } from "react";
import { SafeAreaView, View, FlatList, StyleSheet, Text, StatusBar, Dimensions, Button } from "react-native";

const DATA = [
    {
        id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba",
        title: "First Item",
    },
    {
        id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f63",
        title: "Second Item",
    },
    {
        id: "58694a0f-3da1-471f-bd96-145571e29d72",
        title: "Third Item",
    },
    {
        id: "58694a0f-3da1-471f-bd96-145571e29d72",
        title: "Third Item",
    },
    {
        id: "58694a0f-3da1-471f-bd96-145571e29d72",
        title: "Third Item",
    },
    {
        id: "58694a0f-3da1-471f-bd96-145571e29d72",
        title: "Third Item",
    },
    {
        id: "58694a0f-3da1-471f-bd96-145571e29d72",
        title: "Third Item",
    },
];


export function FlatListTest(props: any) {
    const renderItem = ({ item, index }: any) => (
        <View style={[styles.item, { width: Dimensions.get("screen").width / column }]}>
            <Text style={styles.title}>{item.title}</Text>
        </View>
    );

    const [column, setColumn] = useState(1);

    return (
        <SafeAreaView style={styles.container}>
            <FlatList
                style={{ marginRight: 0 }}
                numColumns={column}
                key={column}
                data={DATA}
                renderItem={({ item, index }) => {
                    return (
                        renderItem({
                            item: item,
                            index: index,
                        })
                    );
                }}
                keyExtractor={item => item.id}
            />
            <View style={{
                flexDirection: "row",
                width: "100%",
                justifyContent: "space-between",
            }}>
                <View style={{ width: "33%", margin: 5 }}>
                    <Button
                        title={"1"}
                        onPress={() => {

                            setColumn(1);
                        }}
                    />
                </View>
                <View style={{ width: "33%", margin: 3 }}>
                    <Button
                        title={"2"}
                        onPress={() => {
                            setColumn(2);
                        }}
                    />
                </View>
                <View style={{ width: "33%", margin: 3 }}>
                    <Button

                        title={"3"}
                        onPress={() => {
                            setColumn(3);
                        }}
                    />
                </View>
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
    },
    item: {
        backgroundColor: "#f9c2ff",
        padding: 1,
        marginVertical: 1,
        marginHorizontal: 1,
    },
    title: {
        fontSize: 32,
        textAlign: "center",
    },
});
