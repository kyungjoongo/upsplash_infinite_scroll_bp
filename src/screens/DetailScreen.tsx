// @flow
import * as React from "react";
import { Button, SafeAreaView, Text, TextInput, View } from "react-native";
import { useObserver } from "mobx-react-lite";
import globalState from "../controllers/SharedController";
import { sharedStyles } from "../styles/SharedStyles";
import sharedService from "../services/SharedService";

type Props = {};

export function DetailScreen(props: any) {
    return useObserver(() => (
        <View style={{ flex: 1 }}>
            <View style={{ flex: 1, alignItems: "center", justifyContent: "center"/* backgroundColor: "white"*/ }}>

                <Text style={{ color: "white" }}> DetailSCreen</Text>
                <Button title={"back"} onPress={() => props.navigation.goBack()} />
                <View style={{ height: 100 }} />
                <Text style={{ color: "white" }}>
                    {globalState.counter}
                </Text>
                <Button title={"incrementCount"} onPress={() => {
                    globalState.incrementDoubleCount();
                }} />
            </View>
        </View>
    ));
}
