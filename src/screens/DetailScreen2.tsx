// @flow
import * as React from "react";
import { ActivityIndicator, Alert, Button, Image, SafeAreaView, ScrollView, Text, TextInput, View } from "react-native";
import { useObserver } from "mobx-react-lite";
import globalState from "../controllers/SharedController";
import { sharedStyles } from "../styles/SharedStyles";
import sharedService from "../services/SharedService";
import storage from "@react-native-firebase/storage";
import { useEffect } from "react";
import { launchCamera, launchImageLibrary } from "react-native-image-picker";
import sharedController from "../controllers/SharedController";

type Props = {};

export function DetailScreen2(props: any) {

    useEffect(() => {
        getAllImages();

    }, []);

    async function getAllImages() {
        let result = await storage().ref("public2/").listAll();
        let accum: any = [];
        let itemList = result.items;
        for (let i = 0; i < itemList.length; i++) {
            let result = await itemList[i].getDownloadURL();
            console.log("resultresult-===>", result);
            accum.push(result);
        }
        console.log("accumaccum-===>", accum);
        sharedController.setVideoList(accum);

    }

    async function uploadImage() {
        const result = await launchImageLibrary({
            mediaType: "photo",
            includeBase64: false,
        });

        console.log("temp-===>", result.assets[0].fileName);
        console.log("temp-===>", result.assets[0].uri);

        let fileName = result.assets[0].fileName;

        sharedController.loading = true;
        const storageRef = storage().ref("public2/" + fileName);

        let filePath = result.assets[0].uri;

        let task = storageRef.putFile(filePath);

        task.on("state_changed", taskSnapshot => {
            console.log(`${taskSnapshot.bytesTransferred} transferred 
  out of ${taskSnapshot.totalBytes}`);
        });
        task.then(() => {
            Alert.alert("이미지 업로드 되었어요~~");
            sharedController.loading = false;
            getAllImages();
        });

    }


    return useObserver(() => (
        <SafeAreaView style={{ backgroundColor: "black", flex: 1 }}>
            <View>
                <Button
                    title={"picker"}
                    onPress={async () => {
                        uploadImage();
                    }}
                />
            </View>
            {sharedController.loading && <ActivityIndicator />}
            <ScrollView style={{ height: 500 }}>
                {sharedController.videoList.map((item: any, index: any) => {

                    return (
                        <View>
                            <Text>{item}</Text>
                            <Image source={{ uri: item }} style={{ width: "100%", height: 300 }} />
                        </View>
                    );
                })}
            </ScrollView>
        </SafeAreaView>

    ));
}
