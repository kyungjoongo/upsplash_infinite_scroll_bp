// @flow
import * as React from "react";
import { Button, SafeAreaView, Text, TextInput, View } from "react-native";
import { useObserver } from "mobx-react-lite";
import globalState from "../controllers/SharedController";
import { sharedStyles } from "../styles/SharedStyles";
import sharedService from "../services/SharedService";

type Props = {};

export function DetailScreen5(props: any) {
    return useObserver(() => (
        <SafeAreaView style={{ backgroundColor: "black", flex: 1 }}>
            <View style={{ height: 50 }}>
                <TextInput
                    style={[sharedStyles.input,
                        {
                            backgroundColor: sharedService.isDarkMode() ? "white" : "#cbc3c3",
                            color: !sharedService.isDarkMode() ? "black" : "black",
                        }]}
                    /*onChangeText={onChangeInputText}
                    value={inputText}
                    onSubmitEditing={handleSubmitEditing}
                    onFocus={() => {
                        setTextInputFocused(true);
                    }}
                    onEndEditing={() => {
                        setTextInputFocused(false);
                    }}*/
                    numberOfLines={1}
                    clearButtonMode={"always"}
                />
            </View>
            <View>
                <Text>
                    DetailScreen5
                </Text>
            </View>
        </SafeAreaView>

    ));
}
