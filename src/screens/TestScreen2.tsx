import React, { useEffect, useState } from "react";
import {
    AccessibilityInfo,
    ActivityIndicator,
    Button,
    Dimensions,
    SafeAreaView,
    ScrollView,
    Text,
    View,
} from "react-native";
import { RootSiblingParent } from "react-native-root-siblings";
import { useObserver } from "mobx-react-lite";
import sharedController from "../controllers/SharedController";

const flatListHeight = Dimensions.get("window").height * 0.9;

export default function TestScreen2(props: any) {
    const [imageList, setImageList] = useState([]);

    useEffect(() => {
        init();
    }, []);

    async function init() {
        let _result = await sharedController.getList();
        console.log("temp-===>", _result);
    }


    return useObserver(() => (
        <RootSiblingParent>
            <SafeAreaView
                style={{
                    flex: 1,
                    backgroundColor: "black",
                }}
            >

                {sharedController.loading && <ActivityIndicator />}
                <ScrollView style={{ height: 400 }}>
                    {sharedController.arrList.map((item: any, index: any) => {
                        return (
                            <View key={index.toString()}>
                                <Text>{item.title}</Text>
                            </View>
                        );
                    })}
                </ScrollView>
                <Button
                    title={"incrementDouble"}
                    onPress={() => {
                        sharedController.incrementDoubleCount();
                    }}
                >
                </Button>
                <Button
                    title={"pushasdasd"}
                    onPress={() => {
                        props.navigation.push("/MainScreen");
                    }}
                >
                </Button>

            </SafeAreaView>
        </RootSiblingParent>
    ));
}
