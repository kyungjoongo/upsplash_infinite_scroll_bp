import React, { useEffect, useRef, useState } from "react";
import { ActivityIndicator, Alert, Dimensions, SafeAreaView, Text, View } from "react-native";
import GestureRecognizer from "react-native-swipe-detect";
import ImageModal from "../components/ImageModal";
import ImageFlatList from "../components/ImageFlatList";
import { sharedStyles } from "../styles/SharedStyles";
import TopSearchBar from "../components/TopSearchBar";
import sharedService from "../services/SharedService";
import { upsplashApiKey } from "../constants/Constants";
import { RootSiblingParent } from "react-native-root-siblings";

const flatListHeight = Dimensions.get("window").height * 0.9;

export default function MainScreen() {
    const [imageList, setImageList] = useState([]);
    const [loading, setLoading] = useState(false);
    const [bottomLoading, setBottomLoading] = useState(false);
    const [currentPage, setCurrentPage] = useState(1);
    const [loadingMore, setLoadingMore] = useState(true);
    const [modalVisible, setModalVisible] = useState(false);
    const [textInputFocused, setTextInputFocused] = useState(false);
    const [currentItem, setCurrentItem]: any = useState(undefined);
    const [currentIdx, setCurrentIdx]: any = useState(0);
    const flatListRef = useRef(null);
    const [inputText, onChangeInputText] = React.useState("");
    const [colCount, setColCount] = useState(1);

    useEffect(() => {
        getInitList();
    }, []);

    async function getInitList() {
        try {
            setLoading(true);
            let _imageList = await sharedService.getImageList({ currentPage, inputText, key: upsplashApiKey });
            setImageList(_imageList);
            setCurrentPage(currentPage + 1);
            setLoading(false);
        } catch (e: any) {
            sharedService.showToast("getInitList error==>" + e.toString());
            setLoading(false);
        }
    }

    const handleLoadMore = async () => {
        let mergeList = [];
        if (loadingMore) {
            setLoadingMore(false);
            try {
                setBottomLoading(true);
                let _moreImageList = await sharedService.getImageList({ currentPage, inputText, key: upsplashApiKey });
                mergeList = imageList.concat(_moreImageList);
                setImageList(mergeList);
                setCurrentPage(currentPage + 1);
            } catch (e: any) {
                sharedService.showToast("handleLoadMore error==>" + e.toString());
            } finally {
                //todo:데이터가 더이상 없을 경우
                if (mergeList.length === 0) {
                    sharedService.showToast("no more data");
                    setBottomLoading(false);
                    setLoadingMore(false);
                } else {
                    setBottomLoading(false);
                    setLoadingMore(true);
                }
            }
        }
    };

    const handleSubmitEditing = () => {
        onChangeInputText(inputText);
        setCurrentPage(1);
        setCurrentIdx(0);
        setImageList([]);
        getInitList();
    };


    const handleOnSwipeRight = () => {
        if (modalVisible) {
            let prevItem: any = imageList[currentIdx - 1];
            if (prevItem !== undefined) {
                setCurrentIdx(currentIdx - 1);
                setCurrentItem(prevItem);
            } else {
                Alert.alert("this is most first picture");
            }
        }
    };

    const handleOnSwipeLeft = () => {
        if (modalVisible) {
            let nextItem: any = imageList[currentIdx + 1];
            if (nextItem !== undefined) {
                setCurrentIdx(currentIdx + 1);
                setCurrentItem(nextItem);
            } else {
                Alert.alert("no more next picture");
            }
        }
    };

    const configGestureRecognizer = {
        velocityThreshold: 0.3,
        directionalOffsetThreshold: 80,
    };

    return (
        <RootSiblingParent>
            <SafeAreaView
                style={{
                    flex: 1,
                }}
            >
                <View style={{ marginTop: sharedService.hasNotch() ? 50 : 0 }}>
                    <TopSearchBar
                        onChangeInputText={onChangeInputText}
                        inputText={inputText}
                        handleSubmitEditing={handleSubmitEditing}
                        setTextInputFocused={setTextInputFocused}
                        colCount={colCount}
                        setColCount={setColCount}
                    />
                </View>
                <View style={{ marginTop: 70 }}>
                    {!loading &&
                    <ImageFlatList
                        flatListRef={flatListRef}
                        imageList={imageList}
                        setCurrentItem={setCurrentItem}
                        setCurrentIdx={setCurrentIdx}
                        setModalVisible={setModalVisible}
                        handleLoadMore={handleLoadMore}
                        flatListHeight={flatListHeight}
                        bottomLoading={bottomLoading}
                        colCount={colCount}
                        setColCount={setColCount}
                    />}
                </View>
                <GestureRecognizer
                    onSwipeLeft={handleOnSwipeLeft}//@todo: next
                    onSwipeRight={handleOnSwipeRight}//@todo: prev
                    onSwipeDown={() => {
                        setModalVisible(false);
                        flatListRef.current.scrollToIndex({
                            animated: true,
                            index: colCount === 1 ? currentIdx : Math.floor(currentIdx / 2),
                        });
                    }}
                    config={configGestureRecognizer}
                    style={{
                        flex: 1,
                    }}
                >
                    <ImageModal
                        modalVisible={modalVisible}
                        setModalVisible={setModalVisible}
                        currentIdx={currentIdx}
                        currentItem={currentItem}
                        flatListRef={flatListRef}
                        colCount={colCount}
                        setColCount={setColCount}
                    />
                </GestureRecognizer>
                {loading &&
                <View style={sharedStyles.loadingContainer}>
                    <ActivityIndicator color={"red"} size={"large"} />
                </View>
                }

            </SafeAreaView>
        </RootSiblingParent>
    );
}
