// @flow
import * as React from "react";
import { Dimensions, FlatList, Text, View } from "react-native";
import { PostVideoSingle } from "./widget/PostVideoSingle";

type Props = {};

export function FeedScreen(props: Props) {

    const array = [1, 2, 3, 4, 5];

    const renderItem = ({ item, index }: any) => {

        return (
            <View
                style={{
                    flex: 1,
                    height: Dimensions.get("window").height ,
                    backgroundColor: index % 2 === 0 ? "blue" : "green",
                    margin: 1,
                }}>
              {/*  <PostVideoSingle />*/}
            </View>
        );
    };

    return (
        <View>
            <FlatList
                data={array}
                renderItem={renderItem}
                pagingEnabled
                removeClippedSubviews={true}
                initialNumToRender={0}
                maxToRenderPerBatch={2}
                //windowSize={4}
                /*viewabilityConfig={{
                    itemVisiblePercentThreshold: 100,
                }}*/
                keyExtractor={(item: any) => item}
                decelerationRate={"normal"}
            />
        </View>
    );
}



