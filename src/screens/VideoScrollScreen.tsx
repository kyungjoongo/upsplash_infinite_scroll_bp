import React, { useEffect, useState } from "react";
import {
    ActivityIndicator,
    Alert,
    Dimensions,
    FlatList,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from "react-native";
import { RootSiblingParent } from "react-native-root-siblings";
import { useObserver } from "mobx-react-lite";
import sharedController from "../controllers/SharedController";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import storage from "@react-native-firebase/storage";
import { launchCamera } from "react-native-image-picker";
import PostVideoSingle from "./widget/PostVideoSingle";

const DATA = [
    {
        id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba",
        url: "https://firebasestorage.googleapis.com/v0/b/camportalsplash.appspot.com/o/shorts%2FA%20Fitness%20Carol%20I%20Make%20your%20workout%20a%20joy%20I%20Peloton%20UK%20I%20Bike.mp4?alt=media&token=61040daf-beb3-4499-8678-dee7c0c44cba",
    },
    {
        id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f63",
        url: "https://firebasestorage.googleapis.com/v0/b/camportalsplash.appspot.com/o/shorts%2FA%20Fitness%20Carol%20I%20Make%20your%20workout%20a%20joy%20I%20Peloton%20UK%20I%20Bike.mp4?alt=media&token=61040daf-beb3-4499-8678-dee7c0c44cba",
    },
    {
        id: "58694a0f-3da1-471f-bd96-145571e29d72",
        url: "https://firebasestorage.googleapis.com/v0/b/camportalsplash.appspot.com/o/shorts%2FA%20Fitness%20Carol%20I%20Make%20your%20workout%20a%20joy%20I%20Peloton%20UK%20I%20Bike.mp4?alt=media&token=61040daf-beb3-4499-8678-dee7c0c44cba",
    },
];
export default function VideoScrollScreen(props: any) {
    const [imageList, setImageList] = useState([]);

    useEffect(() => {
        //init();
        getAllImages();
    }, []);


    async function getAllImages() {
        sharedController.enterLoading = true;
        let result = await storage().ref("shorts/").listAll();
        let finalVideoList: any = [];
        let itemList = result.items;
        for (let i = 0; i < itemList.length; i++) {
            let result = await itemList[i].getDownloadURL();
            finalVideoList.push(result);
        }

        console.log("temp-===>", finalVideoList);
        sharedController.setVideoList(finalVideoList);
        sharedController.enterLoading = false;

        if (sharedController.refList[0] === null) {
            return;
        } else {
            sharedController.refList[0].playAsync();
        }

    }

    async function openImage() {
        const result = await launchCamera({
            mediaType: "video",
            includeBase64: false,
            //videoQuality: "low",
        });

        console.log("temp-===>", result.assets[0].fileName);
        console.log("temp-===>", result.assets[0].uri);

        let fileName = result.assets[0].fileName;
        const storageRef = storage().ref("shorts/" + fileName);

        let filePath = result.assets[0].uri;

        let task = storageRef.putFile(filePath);

        task.on("state_changed", taskSnapshot => {
            console.log(`${taskSnapshot.bytesTransferred} transferred
  out of ${taskSnapshot.totalBytes}`);
        });
        task.then(() => {
            Alert.alert("이미지 업로드 되었어요~~");
            sharedController.loading = false;
            getAllImages();
        });

    }


    const renderItem = ({ item, index }: any) => {
        return (
            <View key={item.toString()}
                  style={{ backgroundColor: "black", height: Dimensions.get("window").height * 1.0 }}>
                <View>
                    <Text>{index.toString()}</Text>
                </View>
                <PostVideoSingle {...props} item={item} index={index} />
            </View>
        );
    };


    return useObserver(() => (
        <RootSiblingParent>
            <SafeAreaView
                style={{
                    flex: 1,
                    backgroundColor: "black",
                }}
            >

                {/*{sharedController.loading && <ActivityIndicator />}*/}
                {sharedController.enterLoading && <ActivityIndicator size={"large"} color={"orange"} />}
                <FlatList
                    onScroll={(event) => {
                        const scrollOffset = event.nativeEvent.contentOffset.y;

                        //console.log("temp-===>", scrollOffset);
                    }}

                    showsHorizontalScrollIndicator={false}
                    onMomentumScrollBegin={(event) => {
                        const prevIndex = Math.round(
                            event.nativeEvent.contentOffset.y /
                            event.nativeEvent.layoutMeasurement.height,
                        );

                        sharedController.setPrevVideoIndex(prevIndex);
                        if (sharedController.refList[sharedController.prevVideoIndex] === null) {
                            return;
                        } else {
                            console.log("sharedController.prevVideoIndex-===>", sharedController.prevVideoIndex);
                            sharedController.refList[sharedController.prevVideoIndex].stopAsync();
                        }

                    }}
                    onMomentumScrollEnd={(event) => {

                        const currentIndex = Math.round(
                            event.nativeEvent.contentOffset.y /
                            event.nativeEvent.layoutMeasurement.height,
                        );
                        // work with: index
                        //console.log("temp-===>", index);
                        sharedController.setCurrentVideoIndex(currentIndex);
                        if (sharedController.refList[currentIndex] === null) {
                            return;
                        } else {
                            sharedController.refList[currentIndex].playAsync();


                        }


                    }}
                    /* removeClippedSubviews={true}
                     viewabilityConfig={{
                         itemVisiblePercentThreshold: 100,
                     }}
                     */
                    //windowSize={4}
                    initialNumToRender={20}
                    //maxToRenderPerBatch={10}
                    //disableIntervalMomentum={true}
                    data={sharedController.videoList}
                    renderItem={renderItem}
                    keyExtractor={item => item.id}
                    horizontal={false}
                    snapToAlignment={"start"}
                    snapToInterval={Dimensions.get("window").height}
                    decelerationRate={"fast"}
                    pagingEnabled={true}
                />
                <View style={{ position: "absolute", bottom: 70, right: 25 }}>
                    <TouchableOpacity>
                        <FontAwesome name={"thumbs-up"} style={{ fontSize: 30 }} />
                    </TouchableOpacity>
                    <View style={{ height: 30 }} />
                    <TouchableOpacity>
                        <FontAwesome name={"thumbs-down"} style={{ fontSize: 30 }} />
                    </TouchableOpacity>
                    <View style={{ height: 30 }} />
                    <TouchableOpacity>
                        <FontAwesome name={"comment"} style={{ fontSize: 30 }} />
                    </TouchableOpacity>
                    <View style={{ height: 30 }} />
                    <TouchableOpacity onPress={() => {
                        openImage();
                    }}>
                        <FontAwesome name={"plus"} style={{ transform: [{ rotateY: "360deg" }], fontSize: 30 }} />
                    </TouchableOpacity>
                    <View style={{ height: 30 }} />
                </View>
            </SafeAreaView>
        </RootSiblingParent>
    ));
}
// Later on in your styles..
var styles = StyleSheet.create({
    backgroundVideo: {
        position: "absolute",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
});

{/*<ScrollView style={{ height: 400 }}>
                    {sharedController.arrList.map((item: any, index: any) => {
                        return (
                            <View key={index.toString()}>
                                <Text>{item.title}</Text>
                            </View>
                        );
                    })}
                </ScrollView>*/
}
/*
<Button
    title={"increment Double"}
    onPress={() => {
        sharedController.incrementDoubleCount();
    }}
>
</Button>
<Button
    title={"push mainScreen"}
    onPress={() => {
        props.navigation.push("/MainScreen");
    }}
>
</Button>*/
