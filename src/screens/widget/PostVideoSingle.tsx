// @flow
import * as React from "react";
import sharedController from "../../controllers/SharedController";
import { Alert, Dimensions, TouchableOpacity, View } from "react-native";
import { useEffect, useRef, useState } from "react";
import { Video, AVPlaybackStatus } from "expo-av";
import { useObserver } from "mobx-react-lite";
import { observer } from "mobx-react";

type Props = {
    item: any,
    index: any,
    videoRef: any,
};

type State = {
    isPlaying: boolean,
};

//https://test-videos.co.uk/vids/bigbuckbunny/mp4/h264/1080/Big_Buck_Bunny_1080_10s_1MB.mp4
class PostVideoSingle extends React.Component<Props, State> {

    constructor(props: any) {
        super(props);

        this.state = {
            isPlaying: false,
        };
    }

    render() {

        return <TouchableOpacity
            key={this.props.index.toString()}
            activeOpacity={1.0}
            onPress={async () => {

                const status = await sharedController.refList[this.props.index].getStatusAsync();

                if (status.shouldPlay) {
                    sharedController.refList[this.props.index].stopAsync();
                } else {
                    sharedController.refList[this.props.index].playAsync();
                }

            }}

        >
            <Video
                ref={c => {
                    sharedController.refList[this.props.index] = c;
                }}
                shouldPlay={this.state.isPlaying}
                source={{ uri: this.props.item }}
                usePoster={true}
                posterSource={{ uri: this.props.item }}
                isLooping={true}
                resizeMode={Video.RESIZE_MODE_COVER}
                style={{
                    height: Dimensions.get("window").height,
                    width: "100%",
                }}

            />

        </TouchableOpacity>;
    }
};

export default observer(PostVideoSingle);
