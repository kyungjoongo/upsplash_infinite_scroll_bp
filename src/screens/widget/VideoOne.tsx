import { ActivityIndicator, Dimensions, TouchableOpacity, View } from "react-native";
import Video from "react-native-video";
import React, { useEffect, useRef, useState } from "react";
import { useObserver } from "mobx-react-lite";
import sharedController from "../../controllers/SharedController";


type Props = {
    url: any,

};
type State = {
    paused: boolean,

};


export default function VideoOne(props: any) {

    const [paused, setPaused] = useState(true);

    useEffect(() => {
        //setPaused(false)
    }, []);

    return useObserver(() => (
        <TouchableOpacity
            activeOpacity={0.9}
            onPress={() => {
                setPaused(!paused);
            }}
            style={{
                width: Dimensions.get("window").width, justifyContent: "center",
                alignItems: "center",
            }}>
            {/*{sharedController.videoLoading && <View style={{ position: "absolute", top: "50%", left: "48%" }}>
                <ActivityIndicator color={'orange'} />
            </View>}*/}
            <Video
                onLoadStart={() => {
                    sharedController.videoLoading = true;
                }}

                /* onReadyForDisplay={() => {

                 }}*/

                paused={paused}
                playInBackground={false}
                preventsDisplaySleepDuringVideoPlayback={true}
                //@ts-ignore
                source={{ uri: props.item }}
                posterResizeMode={'cover'}
                poster={props.item}
                /*ref={c => {
                    this.player = c;
                }}*/

                onLoad={() => {
                    //sharedController.videoLoading = false;
                    //setPaused(!paused);
                    //this.player.
                    if (props.index === sharedController.currentVideoIndex) {
                        //alert('일치하네')
                        setPaused(false);
                    } else {
                        setPaused(true);
                    }
                }}

                resizeMode={"cover"}
                style={{
                    height: Dimensions.get("window").height * 0.7,
                    width: "100%",
                }} />
        </TouchableOpacity>
    ));
}
