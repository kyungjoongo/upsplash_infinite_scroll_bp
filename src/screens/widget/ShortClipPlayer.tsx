// @ts-ignore
let ShortVideo = class extends (require('react-native-video').default) {
    cacheOptions() { return {size: 10, expiresIn: 1200, key: 'shortVideoCache'} };
}
export default ShortVideo;
