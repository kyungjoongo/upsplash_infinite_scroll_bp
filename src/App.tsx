import { createStackNavigator } from "@react-navigation/stack";
import VideoScrollScreen from "./screens/VideoScrollScreen";
import { NavigationContainer } from "@react-navigation/native";
import React from "react";
import { DetailScreen2 } from "./screens/DetailScreen2";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import { FirebaseTestScreen } from "./screens/FirebaseTestScreen";
import { Dimensions, Text, View } from "react-native";
import { FeedScreen } from "./screens/FeedScreen";

const Stack = createStackNavigator();

const HomeStack = () => (
    <Stack.Navigator
        screenOptions={{
            headerShown: false,
        }}
    >
        <Stack.Screen name="/FirebaseTestScreen" component={FirebaseTestScreen} />
        <Stack.Screen name="Test" component={VideoScrollScreen} />
        <Stack.Screen name="FeedScreen" component={FeedScreen} />

    </Stack.Navigator>
);

/*const DetailStack = () => (
    <Stack.Navigator
        screenOptions={{
            headerShown: false,
        }}
    >
        <Stack.Screen name="/DetailScreen2" component={DetailScreen2} />
        <Stack.Screen name="/DetailScreen3" component={DetailScreen3} />
    </Stack.Navigator>
);*/

const Tab = createMaterialBottomTabNavigator();


const DATA = [
    {
        id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba",
        url: "https://firebasestorage.googleapis.com/v0/b/camportalsplash.appspot.com/o/%E1%84%80%E1%85%A9%E1%86%AF%E1%84%87%E1%85%A1%E1%86%AB%E1%84%90%E1%85%A9%E1%86%BC%E1%84%8C%E1%85%B3%E1%86%BC%E1%84%8B%E1%85%A6%20%E1%84%8C%E1%85%A9%E1%87%82%E1%84%8B%E1%85%B3%E1%86%AB%20%E1%84%89%E1%85%B3%E1%84%90%E1%85%B3%E1%84%85%E1%85%A6%E1%84%8E%E1%85%B5%E1%86%BC%E3%85%A3%E1%84%8C%E1%85%A1%E1%86%B7%E1%84%8B%E1%85%AF%E1%86%AB%E1%84%83%E1%85%A9%E1%86%BC%20%E1%84%8B%E1%85%A2%E1%86%AB%E1%84%8A%E1%85%A5%E1%84%92%E1%85%B1%E1%84%90%E1%85%B3%E1%84%82%E1%85%B5%E1%84%89%E1%85%B3.mp4?alt=media&token=58529ab8-f5e2-4572-a436-93c8a6ef9f5a",
    },
    {
        id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f63",
        url: "https://firebasestorage.googleapis.com/v0/b/camportalsplash.appspot.com/o/%5B%E1%84%80%E1%85%B5%E1%84%86%E1%85%A1%E1%84%85%E1%85%A1%E1%86%BC%E1%84%90%E1%85%B5%E1%84%87%E1%85%B5%5D%20%E1%84%80%E1%85%B5%E1%86%B7%E1%84%8B%E1%85%A1%E1%84%85%E1%85%A1%E1%86%BC%E1%84%8B%E1%85%B4%20%E1%84%86%E1%85%A9%E1%84%83%E1%85%B3%E1%86%AB%20%E1%84%80%E1%85%A5%E1%86%BA%20TMI%20%E1%84%8B%E1%85%B5%E1%86%AB%E1%84%90%E1%85%A5%E1%84%87%E1%85%B2%20I%2050%E1%84%86%E1%85%AE%E1%86%AB%2050%E1%84%83%E1%85%A1%E1%86%B8%F0%9F%92%99.mp4?alt=media&token=67521dfb-9443-40dd-8dde-affa46193fb4",
    },
    {
        id: "58694a0f-3da1-471f-bd96-145571e29d72",
        url: "https://firebasestorage.googleapis.com/v0/b/camportalsplash.appspot.com/o/%E1%84%8B%E1%85%A7%E1%86%AB%E1%84%87%E1%85%A9%E1%86%BC%2010%25%20%E1%84%8B%E1%85%A9%E1%86%AF%E1%84%85%E1%85%A1%E1%86%BB%E1%84%8B%E1%85%A5~%F0%9F%92%B8%20%E1%84%89%E1%85%A5%E1%86%AF%E1%84%87%E1%85%B5%E1%84%8A%E1%85%A2%E1%86%B7%E1%84%8B%E1%85%B5%20%E1%84%8B%E1%85%A1%E1%86%AF%E1%84%85%E1%85%A7%E1%84%8C%E1%85%AE%E1%84%82%E1%85%B3%E1%86%AB%20%E1%84%8B%E1%85%AF%E1%86%AF%E1%84%80%E1%85%B3%E1%86%B8%2C%20%E1%84%8B%E1%85%A7%E1%86%AB%E1%84%87%E1%85%A9%E1%86%BC%20%E1%84%80%E1%85%AA%E1%86%AB%E1%84%85%E1%85%A7%E1%86%AB%20%E1%84%80%E1%85%B5%E1%84%8E%E1%85%A9%20%E1%84%8B%E1%85%A7%E1%86%BC%E1%84%8B%E1%85%A5%E1%84%91%E1%85%AD%E1%84%92%E1%85%A7%E1%86%AB%F0%9F%91%8D%20%20%20%E1%84%89%E1%85%B5%E1%86%AF%E1%84%8C%E1%85%A5%E1%86%AB%20%E1%84%8B%E1%85%A7%E1%86%BC%E1%84%8B%E1%85%A5%20%E1%84%91%E1%85%AD%E1%84%92%E1%85%A7%E1%86%AB%20%20%20%E1%84%8B%E1%85%A3%E1%84%82%E1%85%A1%E1%84%83%E1%85%AE%20%20%20%E1%84%8B%E1%85%A7%E1%86%BC%E1%84%8B%E1%85%A5%E1%84%92%E1%85%AC%E1%84%92%E1%85%AA%20%20%20%E1%84%92%E1%85%A1%E1%84%85%E1%85%AE10%E1%84%87%E1%85%AE%E1%86%AB%E1%84%8B%E1%85%A7%E1%86%BC%E1%84%8B%E1%85%A5%20l.mp4?alt=media&token=c279e6e3-791a-474a-bf93-3401175296e8",
    },
];

export default class App extends React.Component {

    _scrollView: any = null;

    rednerItem = (index:any) => {

        return (
            <View style={{
                height: Dimensions.get("window").height * 0.95,
                backgroundColor: index % 2 === 0 ? "red" : "blue",
            }}>
                <Text>sdlkfsldkflk</Text>
                <Text>sdlkfsldkflk</Text>
                <Text>sdlkfsldkflk</Text>
                <Text>sdlkfsldkflk</Text><Text>sdlkfsldkflk</Text>
                <Text>sdlkfsldkflk</Text>
                <Text>sdlkfsldkflk</Text>
                <Text>sdlkfsldkflk</Text>
            </View>
        );
    };

    render() {
        return (
            <NavigationContainer>
                {/**/}
                <HomeStack/>
            </NavigationContainer>
        );
    }
}

/*

<Tab.Navigator
    initialRouteName="Home"
    activeColor="#f0edf6"
    inactiveColor="#3e2465"
    barStyle={{ backgroundColor: "#694fad" }}
>

    <Tab.Screen
        name="Feed"
        component={HomeStack}
        options={{
            tabBarLabel: "Home",
            tabBarIcon: ({ color }) => (
                <FontAwesome name="home" color={color} size={26} />
            ),
        }}
    />
    {/!*<Tab.Screen
                    name="asdasd"
                    component={DetailScreen2}
                    options={{
                        tabBarLabel: "DetailScreen2",
                        tabBarIcon: ({ color }) => (
                            <FontAwesome name="bell" color={color} size={26} />
                        ),
                    }}

                />

                <Tab.Screen
                    name="DetailScreen3"
                    component={DetailStack}
                    options={{
                        tabBarLabel: "Updates3",
                        tabBarIcon: ({ color }) => (
                            <FontAwesome name="plus" color={color} size={26} />
                        ),
                    }}
                />
                <Tab.Screen
                    name="DetailScreen4"
                    component={DetailScreen4}
                    options={{
                        tabBarLabel: "Updates4",
                        tabBarIcon: ({ color }) => (
                            <FontAwesome name="comment" color={color} size={26} />
                        ),
                    }}
                />
                <Tab.Screen
                    name="DetailScreen5"
                    component={DetailScreen5}
                    options={{
                        tabBarLabel: "Updates5",
                        tabBarIcon: ({ color }) => (
                            <FontAwesome name="star" color={color} size={26} />
                        ),
                    }}
                />*!/}
</Tab.Navigator>*/
