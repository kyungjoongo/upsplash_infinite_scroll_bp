import { Platform, StyleSheet } from "react-native";
import sharedService from "../services/SharedService";
import { isIphoneX } from "react-native-iphone-x-helper";


export const sharedStyles = StyleSheet.create({
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,
        zIndex: 99999,
    },
    modalTitle: {
        fontSize: 16,
        marginLeft: 10,
        //fontFamily: Platform.OS === "android" ? "baby_girl" : null,
        color: sharedService.isDarkMode() ? "white" : "black",
    },
    fab: {
        position: "absolute",
        margin: 16,
        right: 0,
        bottom: 0,
    },
    searchBtnContainer: {
        flex: .2,
        marginRight: 10,
        alignSelf: "center",
        zIndex: 999999,
        marginTop: 0,
    },
    loadingContainer: { position: "absolute", top: "45%", left: "48%" },
    imageOneContainer: {
        height: "auto",
        backgroundColor: sharedService.isDarkMode() ? "black" : "white",
        marginVertical: 0,
    },
    imageOne: {
        marginVertical: 3,
        marginHorizontal: 1,
        borderRadius: 5,
    },
    modalTitleContainer: {
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
        paddingTop: isIphoneX() ? 50 : 0,
        height: isIphoneX() ? 105 : 55,
        backgroundColor: sharedService.isDarkMode() ? "black" : "white",
    },
});
