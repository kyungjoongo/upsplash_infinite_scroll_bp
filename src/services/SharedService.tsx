import * as React from "react";
import axios from "axios";
import { Alert, Appearance, Platform, StatusBar } from "react-native";
import Toast from "react-native-root-toast";


type ParamType = {
    currentPage: any,
    inputText: any,
    key: any,
}

export class SharedService {
    async getImageList({ currentPage, inputText, key }: ParamType) {
        try {
            let _inputText= inputText ==='' ? 'all' : inputText
            try{
                let imageList = await axios.get(`https://api.unsplash.com/search/photos?page=${currentPage}&query=${_inputText}&client_id=${key}&per_page=10`).then(res => res.data.results);
                return imageList;
            }catch (e) {
                Alert.alert('sdflksdlfksdlkflskdfl')
            }

        } catch (e: any) {
            Alert.alert(e.toString())
            //throw new Error(e);
        }
    }

    isDarkMode() {
        return Appearance.getColorScheme() === "dark";
    }

    hasNotch() {
        let hasNotch = false;
        if (Platform.OS === "ios") {
            //@ts-ignore
            hasNotch = StatusBar.currentHeight > 24;
        }
        return hasNotch;
    }

    showToast(text: string) {
        Toast.show(text, {
            duration: Toast.durations.LONG,
            position: Toast.positions.BOTTOM,
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            onShow: () => {
                // calls on toast\`s appear animation start
            },
            onShown: () => {
                // calls on toast\`s appear animation end.
            },
            onHide: () => {
                // calls on toast\`s hide animation start.
            },
            onHidden: () => {
                // calls on toast\`s hide animation end.
            },
        });
    }

};


const sharedService = new SharedService();

export default sharedService;
