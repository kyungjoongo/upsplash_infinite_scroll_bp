export type TypeUnsplashItem = {
    item: {
        alt_description: any,
        blur_hash: any,
        categories: any,
        color: any,
        created_at: any,
        current_user_collections: any,
        description: any,
        height: any,
        id: any,
        liked_by_user: any,
        likes: any,
        links: any,
        promoted_at: null
        sponsorship: any,
        tags: any,
        topic_submissions: any,
        updated_at: any,
        urls: {
            raw: string,
            full: string,
            regular: string,
            small: string,
            thumb: string,
        }
        user: { id: string, updated_at: string, username: string, name: string, first_name: string, }
        width: number,
    },
    index: number,

};

export type TypeItemOne = {
    alt_description: any,
    blur_hash: any,
    categories: any,
    color: any,
    created_at: any,
    current_user_collections: any,
    description: any,
    height: any,
    id: any,
    liked_by_user: any,
    likes: any,
    links: any,
    promoted_at: null
    sponsorship: any,
    tags: any,
    topic_submissions: any,
    updated_at: any,
    urls: {
        raw: string,
        full: string,
        regular: string,
        small: string,
        thumb: string,
    }
    user: { id: string, updated_at: string, username: string, name: string, first_name: string, }
    width: number,
}
